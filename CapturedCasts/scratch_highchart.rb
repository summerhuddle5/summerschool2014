#!/usr/bin/env ruby

# Scratch ruby written to generate data to paste into
# http://www.highcharts.com/demo/3d-scatter-draggable

require 'csv'

data = [ ]
max_x = nil
max_y = nil
max_z = nil
min_x = nil
min_y = nil
min_z = nil
moments = CSV.read('gary_real_cast.csv')
moments[2..1500].each_with_index do |m,i|
  #time = m[0]
  #acc     = { :x => m[1], :y => m[2], :z => m[3] }
  #gyro    = { :x => m[4], :y => m[5], :z => m[6] }
  next if i % 10 != 0

  offset = 1  # acc
  #offset = 4  #gyro
  #offset = 7  # compass
  x = m[offset+0].to_f
  y = m[offset+1].to_f
  z = m[offset+2].to_f

  if offset == 1
    x = (x * 100).round(2)
    y = (y * 100).round(2)
    z = (z * 100).round(2)
  end


  data << ('[' + x.to_s + ',' + y.to_s + ','  + z.to_s + ']')
  max_x = x if max_x.nil? or x > max_x
  max_y = y if max_y.nil? or y > max_y
  max_z = y if max_z.nil? or z > max_z
  min_x = x if min_x.nil? or x < min_x
  min_y = y if min_y.nil? or y < min_y
  min_z = z if min_z.nil? or z < min_z
end

puts
print "        yAxis: { min: #{min_x.to_i}, max: #{max_x.to_i}, title: null },\n"
print "        xAxis: { min: #{min_y.to_i}, max: #{max_y.to_i}, gridLineWidth: 1 },\n"
print "        zAxis: { min: #{min_z.to_i}, max: #{max_z.to_i} },\n"
print "        legend: { enabled: false },\n"
print "        series: [{\n"
print "            name: 'Reading',\n"
print "            colorByPoint: true,\n"
print "            data: \n"
puts '[' + data.join(",\n") + ']'
