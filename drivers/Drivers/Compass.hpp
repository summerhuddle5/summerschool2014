#pragma once

struct Raw3DSensorData;

class Compass
{
public:
	virtual ~Compass() { }

	virtual void StartDirectionRead() const = 0;
	virtual bool IsReadingComplete() const = 0;
	virtual Raw3DSensorData ReadDirection() const = 0;
};
