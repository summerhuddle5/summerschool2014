#include "Calibrate3DSensor.hpp"
#include "Raw3DSensorData.hpp"

namespace
{
	float ApplyAxisCalibration(short raw, const AxisCalibration & calibration)
	{
		float result;
		result = raw + calibration.bias;
		result *= calibration.scale;
		return result;
	}
}

Calibrate3DSensor::Calibrate3DSensor(const Calibration & calibration)
	: _calibration(calibration)
{
}

Calibrated3DSensorData Calibrate3DSensor::Calibrate(const Raw3DSensorData & raw) const
{
	Calibrated3DSensorData result;

	result.x = ApplyAxisCalibration(raw.x, _calibration.x);
	result.y = ApplyAxisCalibration(raw.y, _calibration.y);
	result.z = ApplyAxisCalibration(raw.z, _calibration.z);

	return result;
}
