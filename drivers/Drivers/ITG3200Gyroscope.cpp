#include "ITG3200Gyroscope.hpp"
#include "HAL/I2C.hpp"
#include "Raw3DSensorData.hpp"

namespace
{
	short ConvertGyro(const unsigned char * buffer)
	{
		short result = buffer[0];
		result <<= 8;
		result |= buffer[1];
		return result;
	}
}

const unsigned char ITG3200_address = 0x68;

ITG3200Gyroscope::ITG3200Gyroscope(I2C & i2c)
	: i2c(i2c)
{
	i2c.SetAddress(ITG3200_address);
	unsigned char initialiseCommand[] = { 0x16, 0x18 };
	i2c.Write(initialiseCommand, sizeof(initialiseCommand));
}

Raw3DSensorData ITG3200Gyroscope::ReadGyroscope() const
{
	i2c.SetAddress(ITG3200_address);

	unsigned char registerAddress[] = { 0x1D };
	i2c.Write(registerAddress, sizeof(registerAddress));

	unsigned char gyroRegisters[6];
	i2c.Read(gyroRegisters, sizeof(gyroRegisters));

	Raw3DSensorData result;
	result.x = ConvertGyro(&gyroRegisters[0]);
	result.y = ConvertGyro(&gyroRegisters[2]);
	result.z = ConvertGyro(&gyroRegisters[4]);

	return result;
}
