#include <fstream>
#include <iostream>
#include <iomanip>
#include <sys/time.h>
#include <string.h>

#include "Drivers/AK8975Compass.hpp"
#include "Drivers/BMA150Accelerometer.hpp"
#include "Drivers/Calibrate3DSensor.hpp"
#include "Drivers/ITG3200Gyroscope.hpp"
#include "Drivers/Raw3DSensorData.hpp"
#include "RaspberryPiHardware/RaspberryPiI2C.hpp"

const Calibration accelerometerCalibration =
{
	{ -14, 1.0f / 128.0f },
	{ 14,  1.0f / 126.0f },
	{ -3,  1.0f / 127.0f }
};

const Calibration gyroscopeCalibration =
{
	{ 102,  1.0f },
	{ -85,  1.0f },
	{ -202, 1.0f }
};

const Calibration compassCalibration =
{
	{ -10, 1.0f },
	{ -65, -1.0f },
	{ -28, 1.0f }
};

//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

int I2CPort(int argc, char* argv[])
{
	for(int i = 1; i < argc; i++)
	{
		if(strcmp(argv[i], "-0") == 0)
			return 0;
	}
	return 1;
}

const char * FindFilename(int argc, char* argv[])
{
	for(int i = 1; i < argc; i++)
	{
		if(argv[i][0] != '-')
			return argv[i];
	}
	return "";
}

//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

struct CSV_xyz
{
	explicit CSV_xyz(const Calibrated3DSensorData & data)
		: data(data)
	{
	}
	Calibrated3DSensorData data;
};

CSV_xyz csv_xyz(const Calibrated3DSensorData & data)
{
	return CSV_xyz(data);
}

std::ostream & operator<<(std::ostream & os, const CSV_xyz & csv)
{
	os << ',' << csv.data.x
	   << ',' << csv.data.y
	   << ',' << csv.data.z;
	return os;
}

//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

struct CSV_xyz_title
{
	explicit CSV_xyz_title(const char * title)
		: title(title)
	{
	}
	const char * title;
};

CSV_xyz_title csv_xyz(const char * title)
{
	return CSV_xyz_title(title);
}

std::ostream & operator<<(std::ostream & os, const CSV_xyz_title & csv)
{
	os << ',' << csv.title << ".x"
	   << ',' << csv.title << ".y"
	   << ',' << csv.title << ".z";
	return os;
}

//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

void WriteCSV_UntilControlC(std::ofstream & ofs, int port)
{
	RaspberryPiI2C i2c(port);
	BMA150Accelerometer accelerometer(i2c);
	ITG3200Gyroscope gyroscope(i2c);
	AK8975Compass compass(i2c);

	Calibrate3DSensor accelerometerConvert(accelerometerCalibration);
	Calibrate3DSensor gyroscopeConvert(gyroscopeCalibration);
	Calibrate3DSensor compassConvert(compassCalibration);

	ofs << std::fixed << std::setw(2) << std::setprecision(2)
	    << "time"
	    << csv_xyz("acceleration")
	    << csv_xyz("gyro")
	    << csv_xyz("direction")
	    << std::endl;

	timeval start;
	gettimeofday(&start, NULL);

	Raw3DSensorData rawDirection = { 0, 0, 0 };
	compass.StartDirectionRead();

	while(true)
	{
		Raw3DSensorData rawAcceleration = accelerometer.ReadAcceleration();
		Raw3DSensorData rawGyro = gyroscope.ReadGyroscope();

		if(compass.IsReadingComplete())
		{
			rawDirection = compass.ReadDirection();
			compass.StartDirectionRead();
		}

		timeval current;
		gettimeofday(&current, NULL);

		const long seconds = current.tv_sec - start.tv_sec;
		const long useconds = current.tv_usec - start.tv_usec;
		const long elapsed = ((seconds) * 1000 + useconds/1000.0) + 0.5;

		Calibrated3DSensorData acceleration = accelerometerConvert.Calibrate(rawAcceleration);
		Calibrated3DSensorData gyro = gyroscopeConvert.Calibrate(rawGyro);
		Calibrated3DSensorData direction = compassConvert.Calibrate(rawDirection);

		ofs << elapsed
		    << csv_xyz(acceleration)
		    << csv_xyz(gyro)
		    << csv_xyz(direction)
		    << std::endl;

		ofs.flush();
	}
}

//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

int main(int argc, char* argv[])
{
	const char * filename = FindFilename(argc, argv);
	if(strcmp(filename, "") == 0)
	{
		std::cout << "Usage: " << argv[0] << " <filename>" << std::endl;
		return 1;
	}
	else
	{
		std::cout << "Started" << std::endl;
		std::ofstream ofs(filename);
		const int port = I2CPort(argc, argv);
		WriteCSV_UntilControlC(ofs, port);
		return 0;
	}
}
