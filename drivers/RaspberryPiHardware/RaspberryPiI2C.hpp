#pragma once

#include "HAL/I2C.hpp"

class RaspberryPiI2C : public I2C
{
public:
	RaspberryPiI2C(int port);
	virtual ~RaspberryPiI2C();
	virtual bool SetAddress(unsigned char address);
	virtual bool Read(void * buffer, int length);
	virtual bool Write(const void * buffer, int length);
private:
	int file;
};

