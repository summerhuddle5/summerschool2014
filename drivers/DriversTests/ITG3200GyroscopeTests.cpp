#include <gtest/gtest.h>

#include "Drivers/ITG3200Gyroscope.hpp"
#include "Drivers/Raw3DSensorData.hpp"
#include "MockHAL/MockI2C.hpp"

const unsigned char deviceAddress = 0x68;

const unsigned char initialiseCommand[] = { 0x16, 0x18 };

static void AddITG3200Initialise(MockI2C & i2c)
{
	i2c.ExpectSetAddress(deviceAddress, true);
	i2c.ExpectWrite(initialiseCommand, sizeof(initialiseCommand), true);
}

TEST(ITG3000Gyroscope, Initialising_the_gyro)
{
	// Given
	const int maxOperations = 10;
	MockI2COperation operations[maxOperations];
	MockI2C i2c(operations, maxOperations);

	AddITG3200Initialise(i2c);

	// When
	ITG3200Gyroscope target(i2c);

	// Then
	i2c.Verify();
}

TEST(ITG3000Gyroscope, Reading_an_gyro_all_axis_0)
{
	// Given
	const int maxOperations = 10;
	MockI2COperation operations[maxOperations];
	MockI2C i2c(operations, maxOperations);
	AddITG3200Initialise(i2c);
	ITG3200Gyroscope target(i2c);

	unsigned char readCommand[] = { 0x1D };
	unsigned char readData[] = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };

	i2c.ExpectSetAddress(deviceAddress, true);
	i2c.ExpectWrite(readCommand, sizeof(readCommand), true);
	i2c.ExpectRead(readData, sizeof(readData), true);

	// When
	Raw3DSensorData result = target.ReadGyroscope();

	// Then
	i2c.Verify();
	EXPECT_EQ(0, result.x);
	EXPECT_EQ(0, result.y);
	EXPECT_EQ(0, result.z);
}

TEST(ITG3000Gyroscope, Reading_an_gyro_with_different_rotations)
{
	// Given
	const int maxOperations = 10;
	MockI2COperation operations[maxOperations];
	MockI2C i2c(operations, maxOperations);
	AddITG3200Initialise(i2c);
	ITG3200Gyroscope target(i2c);

	unsigned char readCommand[] = { 0x1D };
	unsigned char readData[] = { 0x00, 0x01, 0xFF, 0xFF, 0x80, 0x00 };

	i2c.ExpectSetAddress(deviceAddress, true);
	i2c.ExpectWrite(readCommand, sizeof(readCommand), true);
	i2c.ExpectRead(readData, sizeof(readData), true);

	// When
	Raw3DSensorData result = target.ReadGyroscope();

	// Then
	i2c.Verify();
	EXPECT_EQ(1, result.x);
	EXPECT_EQ(-1, result.y);
	EXPECT_EQ(-32768, result.z);
}
